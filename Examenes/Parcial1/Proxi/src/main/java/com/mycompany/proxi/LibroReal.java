/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.proxi;

/**
 *
 * @author HP
 */
public class LibroReal implements ILibro {
    String contenido,titulo,autor,anio;

    public LibroReal(String contenido, String titulo, String autor, String anio) {
        this.contenido = contenido;
        this.titulo = titulo;
        this.autor = autor;
        this.anio = anio;
    }
    @Override
    public void leer() {
        System.out.println(this.toString());
    }

    @Override
    public String toString() {
        return "LibroReal{" + "contenido=" + contenido + ", titulo=" + titulo + ", autor=" + autor + ", anio=" + anio + '}';
    }
    

    public String getContenido() {
        return contenido;
    }

    public void setContenido(String contenido) {
        this.contenido = contenido;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public String getAnio() {
        return anio;
    }

    public void setAnio(String anio) {
        this.anio = anio;
    }
    

    
    
}
