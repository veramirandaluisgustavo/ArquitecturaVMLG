@extends('layouts.app')

@section('content')
<div class="container">
    <h1>Editar Libro</h1>
    <form id="edit-book-form">
        @csrf
        @method('PUT')
        <div>
            <label for="titulo">Título</label>
            <input type="text" id="titulo" name="titulo" value="{{ $book->titulo }}" required>
        </div>
        <div>
            <label for="editorial">Editorial</label>
            <input type="number" id="editorial_id" name="editorial_id" value="{{ $book->editorial_id }}" required>
        </div>
        <div>
            <label for="edicion">Edición</label>
            <input type="number" id="edicion" name="edicion" value="{{ $book->edicion }}" required>
        </div>
        <div>
            <label for="pais">País</label>
            <input type="text" id="pais" name="pais" value="{{ $book->pais }}" required>
        </div>
        <div>
            <label for="precio">Precio</label>
            <input type="number" id="precio" name="precio" value="{{ $book->precio }}" required>
        </div>
        <button type="button" onclick="updateBook({{ $book->id }})">Actualizar</button>
        <div>{{$book->id}}</div>
    </form>
</div>

<script>
    function updateBook(id) {
        const csrfToken = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
        const formData = new FormData(document.getElementById('edit-book-form'));
        const url = '{{ route('libro.update', ':id') }}'.replace(':id', id);
        console.log(url);

        fetch(url, {
            method: 'PUT',
            headers: {
                'X-CSRF-TOKEN': csrfToken,
                'Accept': 'application/json',
                'X-Requested-With': 'XMLHttpRequest'
            },
            body: formData
        })
        .then(response => response.json())
        .then(data => {
            if (data.message) {
                alert(data.message);
            } else {
                alert('Error al actualizar el libro.');
            }
        })
        .catch(error => console.error('Error:', error));
    }
</script>
@endsection