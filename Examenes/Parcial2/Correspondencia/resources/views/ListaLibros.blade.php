@extends('layouts.app')

@section('content')
    <table class="table">
        <thead>
            <tr>
                <th scope="col">Titulo</th>
                <th scope="col">Edicion</th>
                <th scope="col">Pais</th>
                <th scope="col">Precio</th>
                <th scope="col">Editorial</th>
                <th scope="col">opciones</th>
                
            </tr>
        </thead>
        <tbody>
            @foreach ($libros as $dato)
                <tr>
                    <th scope="row">{{$dato['titulo']}}</th>
                    <td>{{$dato['edicion']}}</td>
                    <td>{{$dato['pais']}}</td>
                    <td>{{$dato['precio']}}</td>
                    <td>{{$dato->editorials->nombre}}</td>
                    <td>
                        <div style="display: flex;flex-direction:row">
                            <form action="{{ route('libro.destroy', $dato->id) }}" method="POST" onsubmit="return confirm('¿Está seguro de que desea eliminar este producto?');">
                                @csrf
                                @method('DELETE')
                                <button type="subnit" class="btn btn-primary">eliminar</button>
                            </form>
                            <form action="{{ route('libro.edit', $dato->id) }}" method="POST" onsubmit="return confirm('¿Está seguro de que desea editar?');">
                                @csrf
                                @method('GET')
                                <button type="subnit" class="btn btn-primary">editar</button>
                            </form>
                        </div>
                        
                        
                    </td>
                </tr>
            @endforeach


        </tbody>
    </table>
@endsection
