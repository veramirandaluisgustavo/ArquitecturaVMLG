@extends('layouts.app')

@section('content')
    <table class="table">
        <thead>
            <tr>
                <th scope="col">ID</th>
                <th scope="col">Titulo</th>
                
                <th scope="col">opciones</th>
                
            </tr>
        </thead>
        <tbody>
            @foreach ($editoriales as $dato)
                <tr>
                    <th scope="row">{{$dato['id']}}</th>
                    <td>{{$dato['nombre']}}</td>
      
               
                    <td>
                        <div style="display: flex;flex-direction:row">
                            <form action="{{ route('editorial.destroy', $dato->id) }}" method="POST" onsubmit="return confirm('¿Está seguro de que desea eliminar este producto?');">
                                @csrf
                                @method('DELETE')
                                <button type="subnit" class="btn btn-primary">eliminar</button>
                            </form>
                            <form action="{{ route('editorial.destroy', $dato->id) }}" method="POST" onsubmit="return confirm('¿Está seguro de que desea eliminar este producto?');">
                                @csrf
                                @method('GET')
                                <button type="subnit" class="btn btn-primary">editar</button>
                            </form>
                        </div>
                        
                        
                    </td>
                </tr>
            @endforeach


        </tbody>
    </table>
@endsection
