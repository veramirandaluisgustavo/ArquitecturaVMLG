@extends('layouts.app')
@section('css')
<link rel="stylesheet" href="{{ asset('css/menu.css') }}">
@endsection
@section('content')
<div class="menu">
    <a href="/libro"><button class="menu-button">Lista Libros</button></a>
    <a href="/editorial"><button class="menu-button">Lista Editoriales</button></a>
</div>
@endsection