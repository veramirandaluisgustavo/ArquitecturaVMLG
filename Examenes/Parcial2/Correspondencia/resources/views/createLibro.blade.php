@extends('layouts.app')

@section('content')
<div class="flex justify-center items-center flex-col w-full h-auto rounded-lg">
    <div class="w-1/2 flex justify-center items-center flex-col rounded-lg border border-blue-500">
        <div class="w-1/2">
            <form action="{{ route('products.store') }}" method="post">
                @csrf

                <br>

                <div>
                    <label for="codigo_producto" class="block text-sm font-medium leading-6 text-gray-900">Código Producto</label>
                    <div class="mt-2">
                        <input id="codigo_producto" name="codigo_producto" type="text" required class="block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6">
                    </div>
                </div>

                <div>
                    <label for="libro_id" class="block text-sm font-medium leading-6 text-gray-900">libro</label>
                    <div class="mt-2">
                        <select id="libro_id" name="libro_id" required class="block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6">
                            @foreach ($libros as $libro)
                                <option value="{{ $libro->id }}">{{ $libro->product_nombre }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div>
                    <label for="costo" class="block text-sm font-medium leading-6 text-gray-900">Costo</label>
                    <div class="mt-2">
                        <input id="costo" name="costo" type="number" step="0.01" required class="block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6">
                    </div>
                </div>

                <div>
                    <label for="precio_anterior" class="block text-sm font-medium leading-6 text-gray-900">Precio Anterior</label>
                    <div class="mt-2">
                        <input id="precio_anterior" name="precio_anterior" type="number" step="0.01" required class="block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6">
                    </div>
                </div>

                <div>
                    <label for="precio" class="block text-sm font-medium leading-6 text-gray-900">Precio</label>
                    <div class="mt-2">
                        <input id="precio" name="precio" type="number" step="0.01" required class="block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6">
                    </div>
                </div>

                <div>
                    <label for="ganancia" class="block text-sm font-medium leading-6 text-gray-900">Ganancia</label>
                    <div class="mt-2">
                        <input id="ganancia" name="ganancia" type="number" step="0.01" class="block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6">
                    </div>
                </div>

                <div>
                    <label for="cantidad_disponible" class="block text-sm font-medium leading-6 text-gray-900">Cantidad Disponible</label>
                    <div class="mt-2">
                        <input id="cantidad_disponible" name="cantidad_disponible" type="number" required class="block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6">
                    </div>
                </div>

                <div>
                    <label for="cantidad_total" class="block text-sm font-medium leading-6 text-gray-900">Cantidad Total</label>
                    <div class="mt-2">
                        <input id="cantidad_total" name="cantidad_total" type="number" required class="block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6">
                    </div>
                </div>

                <div>
                    <label for="cantidad_vendida" class="block text-sm font-medium leading-6 text-gray-900">Cantidad Vendida</label>
                    <div class="mt-2">
                        <input id="cantidad_vendida" name="cantidad_vendida" type="number" required class="block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6">
                    </div>
                </div>

                <div>
                    <label for="fecha_vencimiento" class="block text-sm font-medium leading-6 text-gray-900">Fecha de Vencimiento</label>
                    <div class="mt-2">
                        <input id="fecha_vencimiento" name="fecha_vencimiento" type="date" required class="block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6">
                    </div>
                </div>

                <div>
                    <label for="fecha_llegada" class="block text-sm font-medium leading-6 text-gray-900">Fecha de Llegada</label>
                    <div class="mt-2">
                        <input id="fecha_llegada" name="fecha_llegada" type="date" required class="block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6">
                    </div>
                </div>


                <div class="mt-4">
                    <button type="submit" class="flex w-full justify-center rounded-md bg-indigo-600 px-3 py-1.5 text-sm font-semibold leading-6 text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600">Registrar Producto</button>
                </div>
            </form>
            <br>
        </div>
    </div>
</div>
@endsection
