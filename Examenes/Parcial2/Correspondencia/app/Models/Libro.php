<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Libro extends Model
{
    use HasFactory;
    protected $fillable = [
        'titulo',
        'editorial_id',
        'edicion',
        'pais',
        'precio'
    ];
    function editorials(){
        return $this->belongsTo(Editorial::class);
    }
}
