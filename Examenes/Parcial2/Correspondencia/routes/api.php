<?php

use App\Http\Controllers\CorrespondenciaController;
use App\Http\Controllers\EditorialController;
use App\Http\Controllers\LibroController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
//Route::delete('/correspondencia/{correspondencia}',[CorrespondenciaController::class,'destroy'])->name('correspondencia.destroy');
Route::delete('/libro/{libro}',[LibroController::class,'destroy'])->name('libro.destroy');
Route::delete('/editorial/{editorial}',[EditorialController::class,'destroy'])->name('editorial.destroy');
//Route::put('/libro/{id}', [LibroController::class, 'update'])->name('books.update');
//Route::get('/books/{id}/edit', [BookController::class, 'edit'])->name('books.edit');
//Route::put('/books/{id}', [BookController::class, 'update'])->name('books.update');