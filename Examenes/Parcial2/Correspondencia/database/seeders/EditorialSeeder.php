<?php

namespace Database\Seeders;

use App\Models\Editorial;
use App\Models\Libro;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class EditorialSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Editorial::factory()
            ->has(Libro::factory()->count(1000))
            ->count(10)->create();
    }
}
