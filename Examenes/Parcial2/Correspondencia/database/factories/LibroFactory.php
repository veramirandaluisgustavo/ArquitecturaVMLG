<?php

namespace Database\Factories;

use App\Models\Editorial;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Libro>
 */
class LibroFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'titulo'=>fake()->numerify('titulo_######'),
            'editorial_id'=>Editorial::factory(),
            'edicion'=>rand(1,10),
            'pais'=>fake()->country(),
            'precio'=>fake()->randomFloat(2,5,200)
        ];
    }
}
