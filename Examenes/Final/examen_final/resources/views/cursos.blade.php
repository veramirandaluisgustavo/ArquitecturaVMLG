@extends('layouts.app');

@section('content')
<body class="bg-gray-100 p-8">
    <div class="container mx-auto">
        <h1 class="text-3xl font-bold mb-6">Lista de Cursos</h1>
        <div class="mb-4">
            <a href="" class="bg-blue-500 text-white font-bold py-2 px-4 rounded hover:bg-blue-700">
                Agregar Curso
            </a>
        </div>
        <div class="bg-white shadow-md rounded my-6">
            <table class="min-w-full bg-white">
                <thead>
                    <tr>
                        <th class="py-2 px-4 bg-gray-200 text-gray-600 font-bold uppercase text-sm text-left">Nombre</th>
                        <th class="py-2 px-4 bg-gray-200 text-gray-600 font-bold uppercase text-sm text-left">Fecha Inicio</th>
                        <th class="py-2 px-4 bg-gray-200 text-gray-600 font-bold uppercase text-sm text-left">Fecha Final</th>
                        <th class="py-2 px-4 bg-gray-200 text-gray-600 font-bold uppercase text-sm text-left">Tipo</th>
                        <th class="py-2 px-4 bg-gray-200 text-gray-600 font-bold uppercase text-sm text-left">Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    
                    <tr class="border-b">
                        <td class="py-2 px-4"></td>
                        <td class="py-2 px-4"></td>
                        <td class="py-2 px-4"></td>
                        <td class="py-2 px-4"></td>
                        <td class="py-2 px-4">
                            <a href="" class="bg-yellow-500 text-white font-bold py-1 px-2 rounded hover:bg-yellow-700">Editar</a>
                            <form action="" method="POST" class="inline-block">
       
                                <button type="submit" class="bg-red-500 text-white font-bold py-1 px-2 rounded hover:bg-red-700">Eliminar</button>
                            </form>
                        </td>
                    </tr>
           
                </tbody>
            </table>
        </div>
    </div>
</body>
@endsection