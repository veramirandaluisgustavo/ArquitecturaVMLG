@extends('layouts.app');

@section('content')
<body class="bg-gray-100 flex items-center justify-center min-h-screen">
    <div class="bg-white p-8 rounded shadow-md text-center">
        <h1 class="text-2xl font-bold mb-6">Botones con Tailwind CSS</h1>
        <div class="space-x-4">
            <button class="bg-blue-500 text-white font-bold py-2 px-4 rounded hover:bg-blue-700">Botón 1</button>
            <button class="bg-green-500 text-white font-bold py-2 px-4 rounded hover:bg-green-700">Botón 2</button>
            <button class="bg-red-500 text-white font-bold py-2 px-4 rounded hover:bg-red-700">Botón 3</button>
            <button class="bg-yellow-500 text-white font-bold py-2 px-4 rounded hover:bg-yellow-700">Botón 4</button>
        </div>
    </div>
</body>
@endsection
