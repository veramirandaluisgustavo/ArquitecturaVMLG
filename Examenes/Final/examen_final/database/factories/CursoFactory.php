<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Curso>
 */
class CursoFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    
    public function definition(): array
    {
        $tipo = $this->randomTipo();
        return [
            'nombre'=>fake()->numerify('curso_#####'),
            'fecha_inicio'=>fake()->date(),
            'fecha_final'=>fake()->date(),
            'tipo'=> $tipo,
        ];
    }
    public function randomTipo(){
        $tipo = random_int(0,2);
        switch($tipo){
            case 0:return 'seminario';
            case 1:return 'formacion';
            case 2:return 'taller';
        }
        return null;
    }
}
