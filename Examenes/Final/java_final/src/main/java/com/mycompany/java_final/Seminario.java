/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.java_final;
import java.util.ArrayList;
import java.util.Scanner;
/**
 *
 * @author HP
 */
public class Seminario extends Curso {
    
    
    ArrayList<Participante> lista = new ArrayList<Participante>();
    @Override
    public void agregar(){
        Scanner sc =new Scanner(System.in);
        System.out.println("cual es el nombre del participante que desea agregar?");
        String nombre = sc.next();
        System.out.println("cual es el edad del participante que desea agregar?");
        int edad = sc.nextInt();
        System.out.println("cual es el sexo del participante que desea agregar?");
        String sexo = sc.next();
        this.lista.add(new Participante(nombre,edad,sexo));
        System.out.println("participante agregado con exito?");
        
    }

    public Seminario(String nombre, String fechaInicio, String fechaFinal) {
        super(nombre, fechaInicio, fechaFinal);
    }

    
    
}
