/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.java_final;

import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author HP
 */
public class FormacionContinua extends Curso {
    ArrayList<Tema> lista = new ArrayList<>();
    @Override
    public void agregar(){
        
        Scanner sc = new Scanner(System.in);

        System.out.println("cual es el nombre del tema que desea agregar?");
        String nombre = sc.next();
        System.out.println("cual es la descricion del tema que desea agregar?");
        String descripcion = sc.next();
        
        this.lista.add(new Tema(nombre,descripcion));
        System.out.println("Tema agregado con exito?");
        
    }

    public FormacionContinua(String nombre, String fechaInicio, String fechaFinal) {
        super(nombre, fechaInicio, fechaFinal);
    }
    
}
