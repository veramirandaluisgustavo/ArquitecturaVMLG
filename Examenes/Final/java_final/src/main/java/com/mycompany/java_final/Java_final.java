/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package com.mycompany.java_final;

import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author HP
 */
public class Java_final {

    static Scanner sc;
    static ArrayList<Curso> lista = new ArrayList<>();

    public static void main(String[] args) {
        sc = new Scanner(System.in);
        while (true) {
            int respuesta = 0;
            System.out.println("elija una opcion");
            System.out.println("1-generar curso");
            System.out.println("2-agregar tema/actividad/participante");

            respuesta = sc.nextInt();
            switch (respuesta) {
                case 1:
                    generarCurso();
                    break;
                case 2:
                    registrarseCurso();
            }
        }

    }

    static void generarCurso() {
        Curso curso = null;

        System.out.println("que tipo de curso desea crear?");
        System.out.println("1-seminario");
        System.out.println("2-Formacion Continua");
        System.out.println("3-Taller");
        int respuesta = sc.nextInt();
        System.out.println("escriba un nombre para el curso");
        String nombre = sc.next();
        System.out.println("introduzca fecha de inicio");
        String fechaInicio = sc.next();
        System.out.println("introduzca fecha de finalizacion");
        String fechaFinal = sc.next();

        switch (respuesta) {
            case 1:
                curso = new Seminario(nombre, fechaInicio, fechaFinal);
                break;
            case 2:
                curso = new FormacionContinua(nombre, fechaInicio, fechaFinal);
                break;
            case 3:
                curso = new Taller(nombre, fechaInicio, fechaFinal);
                break;
            default:
                System.out.println("no introgujo una opcion correcta");
        }
        if (curso != null) {
            lista.add(curso);
        }

    }

    private static void registrarseCurso() {
        System.out.println("a que curso desea agregar?");
        int indice = 0;
        for (Curso curso : lista) {
            System.out.println(indice + "-" + curso.getNombre());
        }
        int respuesta = sc.nextInt();
        lista.get(respuesta).agregar();
    }
}
