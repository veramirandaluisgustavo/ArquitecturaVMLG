/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.java_final;

import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author HP
 */
public class Taller extends Curso {
    ArrayList<Actividad> lista = new ArrayList<>();
    @Override
    public void agregar(){
        Scanner sc =new Scanner(System.in);
        System.out.println("escrive el nombre de la actividad que deseas agregar");
        String nombre = sc.next();
        this.lista.add(new Actividad(nombre));
        System.out.println("actividad agregado con exito?");
        
    }

    public Taller(String nombre, String fechaInicio, String fechaFinal) {
        super(nombre, fechaInicio, fechaFinal);
    }
    
}
