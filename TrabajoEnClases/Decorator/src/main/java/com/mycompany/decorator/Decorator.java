/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.decorator;

import java.util.Scanner;

/**
 *
 * @author HP
 */
public class Decorator {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int op=1;
        while(op!=2){
            System.out.println("----------BANCO----------");
            System.out.println("1-Abrir cuenta");
            System.out.println("2-salir");
            op=sc.nextInt();
            switch(op){
                case 1:
                    abrirCuenta();
                    break;
                case 2:
                    System.out.println("ADIOS");
                    break;
                default:
                    System.out.println("opcion no valida");
                    break;
            }
            
        }
    }
    private static void abrirCuenta(){
        Scanner sc =new Scanner(System.in);
        int op;
        System.out.println("--------------ABRIR CUENTA------------");
        System.out.println("1-Abrir cuenta corriente");
        System.out.println("2-Abrir cuenta blindada");
        System.out.println("3-Abrir cuenta de ahorro");
        System.out.println("4-cancelar");
        op=sc.nextInt();
        switch(op){
            case 1:
                System.out.println("Cuenta corriente creada");
                break;
            case 2:
                System.out.println("Cuenta blindada creada");
                break;
            case 3:
                System.out.println("cuenta de ahorro creada");
                break;
            case 4:
                System.out.println("operacion cancelada");
                return;
                
            default:
                System.out.println("Opcion no valida");
                return;
               
        }
        
        
    }
}
