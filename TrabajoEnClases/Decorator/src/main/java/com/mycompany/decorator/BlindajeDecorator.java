/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.decorator;

/**
 *
 * @author HP
 */
public class BlindajeDecorator  extends CuentaDecorator{

    public BlindajeDecorator(ICuentaBancaria cuentaBancaria) {
        super(cuentaBancaria);
    }

    
    

    @Override
    public void abrirCuenta(Cuenta c) {
        this.cuentaBancaria.abrirCuenta(c);
        System.out.println("cuenta blindada creada");
    }
    
}
