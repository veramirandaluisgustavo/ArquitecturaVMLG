/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.decorator;

/**
 *
 * @author HP
 */
public abstract class CuentaDecorator implements ICuentaBancaria {
    
    protected ICuentaBancaria cuentaBancaria;

    public CuentaDecorator(ICuentaBancaria cuentaBancaria) {
        this.cuentaBancaria = cuentaBancaria;
    }
    
    
}
