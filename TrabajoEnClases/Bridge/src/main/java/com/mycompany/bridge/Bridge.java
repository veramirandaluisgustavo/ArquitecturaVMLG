/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.bridge;

import java.util.Scanner;

/**
 *
 * @author HP
 */
public class Bridge {

    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int op=1;
        ElaborarAlimento lasania=null;
        while(op!=4){
            System.out.println("-----------------Preparacion de lasania-----------");
            System.out.println("1-Lasania de carne");
            System.out.println("2-Lasania picante");
            System.out.println("3-lasania de verduras");
            System.out.println("4-Salir");
            System.out.println("ELIJA UNA OPCION");
            op=sc.nextInt();
            switch(op){
                case 1:
                    lasania=new ElaborarLasania(new Carne());
                    break;
                case 2:
                    lasania=new ElaborarLasania(new Picante());
                    break;
                case 3:
                    lasania=new ElaborarLasania(new Verdura());
                    break;
                case 4:
                    System.out.println("adios");
                    break;
                default:
                    System.out.println("opcion no valida");
                    break;
            }
            lasania.obtener();
            
        }
    }
}
