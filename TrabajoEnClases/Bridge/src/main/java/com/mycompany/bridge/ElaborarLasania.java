/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.bridge;

/**
 *
 * @author HP
 */
public class ElaborarLasania extends ElaborarAlimento {

    public ElaborarLasania(IElaborar implementador) {
        this.setImplementador(implementador);
        
    }
    
    

    @Override
    public void obtener() {
        System.out.println("Preparando lasania...");
        this.getImplementador().procesar();
        
    }
    
}
