/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.bridge;

/**
 *
 * @author HP
 */
public abstract class ElaborarAlimento {
    IElaborar implementador;
    String nombre;

    public IElaborar getImplementador() {
        return implementador;
    }

    public void setImplementador(IElaborar implementador) {
        this.implementador = implementador;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    public abstract void obtener();
    
    
}
