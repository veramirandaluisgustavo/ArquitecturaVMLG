<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('participantes', function (Blueprint $table) {
            $table->id();
            $table->string('carnet_identidad',12);
            $table->string('nombres',99);
            $table->string('apellidos',30);
            $table->string('sexo',10);
            $table->string('direccion',30);
            $table->string('correo_electronico',50);
            $table->string('celular',15);
            $table->string('ciudad',30);
            $table->timestamps();


        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('participantes');
    }
};
