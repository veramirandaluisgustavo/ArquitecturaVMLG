<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Participante>
 */
class ParticipanteFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $sex=fake()->randomElement(['hombre','mujer']);
        if($sex=='hombre'){
            $name=fake()->name('male');
        }
        else{
            $name=fake()->name('female');

        }
        
        return [
            'carnet_identidad'=>fake()->numerify('c-####'),
            'nombres'=>$name,
            'apellidos'=>fake()->lastName(),
            'sexo'=>$sex,
            'direccion'=>fake()->numerify('c-####'),
            'correo_electronico'=>fake()->email(),
            'celular'=>fake()->shuffle('1239843352123'),
            'ciudad'=>fake()->name(),
        ];
    }
}
