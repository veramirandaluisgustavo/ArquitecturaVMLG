<?php

namespace Database\Factories;

use App\Models\Correspondencia;
use App\Models\Destinatario;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Correspondencia>
 */
class CorrespondenciaFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'fecha' => fake()->date(),
            'remitente' => fake()->numerify('remitente_#####'),
            'asunto' => fake()->numerify('asunto_#####'),
            'cite' => fake()->numerify('cite_#####'),
            'destinatario_id' => Destinatario::factory()
        ];
    }
}
