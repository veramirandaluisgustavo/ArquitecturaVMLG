@extends('layouts.app')
@section('css')
<link rel="stylesheet" href="{{ asset('css/menu.css') }}">
@endsection
@section('content')
<div class="menu">
    <a href="/correspondencia"><button class="menu-button">Lista Correspondencia</button></a>
    <a href="/destinatario"><button class="menu-button">Lista Destinatario</button></a>
</div>
@endsection