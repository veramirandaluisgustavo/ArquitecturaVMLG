@extends('layouts.app')

@section('content')
    <table class="table">
        <thead>
            <tr>
                <th scope="col">fecha</th>
                <th scope="col">remitente</th>
                <th scope="col">asunto</th>
                <th scope="col">cite</th>
                <th scope="col">opciones</th>
                
            </tr>
        </thead>
        <tbody>
            @foreach ($correspondencias as $dato)
                <tr>
                    <th scope="row">{{$dato['fecha']}}</th>
                    <td>{{$dato['remitente']}}</td>
                    <td>{{$dato['asunto']}}</td>
                    <td>{{$dato['cite']}}</td>
                    <td>
                        <div style="display: flex;flex-direction:row">
                            <form action="{{ route('correspondencia.destroy', $dato->id) }}" method="POST" onsubmit="return confirm('¿Está seguro de que desea eliminar este producto?');">
                                @csrf
                                @method('DELETE')
                                <button type="subnit" class="btn btn-primary">eliminar</button>
                            </form>
                            <form action="{{ route('correspondencia.destroy', $dato->id) }}" method="POST" onsubmit="return confirm('¿Está seguro de que desea eliminar este producto?');">
                                @csrf
                                @method('DELETE')
                                <button type="subnit" class="btn btn-primary">editar</button>
                            </form>
                        </div>
                        
                        
                    </td>
                </tr>
            @endforeach


        </tbody>
    </table>
@endsection
