<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Destinatario extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $fillable=[
        'nombre',
        'cargo'
    ];
    public function correspondencias(){
        return $this->hasMany(Correspondencia::class);
    }
}
