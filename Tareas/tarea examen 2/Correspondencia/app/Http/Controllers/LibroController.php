<?php

namespace App\Http\Controllers;

use App\Models\Libro;
use Illuminate\Http\Request;

class LibroController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $libros = Libro::take(100)->get(); 
        //return view('ListaLibros',['libros'=>$libros]);
        return view('ListaLibros')->with('libros',$libros);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Libro $request)
    {
        $request->save();
    }

    /**
     * Display the specified resource.
     */
    public function show(Libro $libro)
    {
        return response()->json($libro);
    }

    /**
     * Show the form for editing the specified resource.
     */
    

    /**
     * Update the specified resource in storage.
     */
    public function edit($id)
    {
        $book = Libro::findOrFail($id);
        return view('editLibro',['book'=>$book]);
    }

    public function update(Request $request, $id)
    {
        
        $book = Libro::findOrFail($id);
        $book->update($request->all());
        return redirect()->route('libro.index');
    }
    

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Libro $libro)
    {
        $libro->delete();
        $libros = Libro::take(100)->get(); 
        return view('ListaLibros',['libros'=>$libros]);
    }
}
