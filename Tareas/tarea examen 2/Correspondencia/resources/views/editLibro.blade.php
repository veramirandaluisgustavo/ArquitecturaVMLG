@extends('layouts.app')

@section('content')
<div class="container">
    <h1>Editar Libro</h1>
    <form id="edit-book-form" method="POST" action="{{route('libro.update',$book->id)}}">
        @csrf
        @method('PUT')
        <div>
            <label for="titulo">Título</label>
            <input type="text" id="titulo" name="titulo" value="{{ $book->titulo }}" required>
        </div>
        <div>
            <label for="editorial">Editorial</label>
            <input type="number" id="editorial_id" name="editorial_id" value="{{ $book->editorial_id }}" required>
        </div>
        <div>
            <label for="edicion">Edición</label>
            <input type="number" id="edicion" name="edicion" value="{{ $book->edicion }}" required>
        </div>
        <div>
            <label for="pais">País</label>
            <input type="text" id="pais" name="pais" value="{{ $book->pais }}" required>
        </div>
        <div>
            <label for="precio">Precio</label>
            <input type="number" id="precio" name="precio" value="{{ $book->precio }}" required>
        </div>
        <button type="submit" ">Actualizar</button>
        <div>{{$book->id}}</div>
    </form>
</div>


@endsection