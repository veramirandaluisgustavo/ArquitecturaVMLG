<?php

use App\Http\Controllers\CorrespondenciaController;
use App\Http\Controllers\EditorialController;
use App\Http\Controllers\LibroController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/libro', [LibroController::class,'index'])->name('libro.index');
Route::get('/editorial', [EditorialController::class,'index']);
Route::get('/editlibro', [EditorialController::class,'index']);
Route::get('/editlibro/{id}/edit', [LibroController::class, 'edit'])->name('libro.edit');
Route::put('/libro/{id}', [LibroController::class, 'update'])->name('libro.update');